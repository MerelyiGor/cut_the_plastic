<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cut-the-plastic');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'mysql');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'R-{0]2<_8$`*Y6#_8E=peukqg|us[h~/l`1ftq{ap.`&]7$19_3=rW_l;UO}C<=U');
define('SECURE_AUTH_KEY',  'b@2-<0ydiKNjV:XSK! @}ov)gpz0/Ts6x-B4NJT_)ij&R,B4PXGGg3@MEM:[#xz/');
define('LOGGED_IN_KEY',    '!#}X#a}Y|qu1_q`hLU6vDbek>K;.W?sU*Jz6~b+=]uu4Fzsu482ruw,dlPb,W*Pw');
define('NONCE_KEY',        ',ql3[_xXyD[5r_(xBYl3XsC{TqeOVB4+:ZRTxBQFb6b`*;0+Jx:IhsAYWtSFa,&r');
define('AUTH_SALT',        'X1)sG8#Q//`n8w6RgpLR(**uLv%8W2c!y-A0c-L8R8d7bUz#:?%BCpCb1c|0VQA2');
define('SECURE_AUTH_SALT', 'YB8MQ8}><l6<A/RsqZyeI!;A*<85Z2UvWVNTF6KqU)B}3n318$5={4_M8V(hAo^b');
define('LOGGED_IN_SALT',   '4f|{= V]rV$Pv#zH|8x+aJhx+E4qJU4IipoS#[xZ>2b+.?/Owh:9BA7F5rfOQ&XE');
define('NONCE_SALT',       '3]1JLh|ONVtR0>p~nfO84u+u#I4ivY*yzFBMR3hOyC?9J` wv/G{$/YTVsU**o7}');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
