<?php
/**
 * The template for displaying the header
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
    <![endif]-->
    <title><?php bloginfo('name'); ?> |
        <?php is_home() ? bloginfo('description') : wp_title(''); ?>
    </title>
    <?php wp_head(); ?>
</head>

<body <?= body_class() ?>>

<header class="header">
    <div class="lg-container">
            <a class="logo" href="/">
                <img src="<?= get_template_directory_uri(); ?>/img/pages/elems/Logo.png" alt="logo">
            </a>
        <div class="hamburger-js">
            <span class="btn-toggle"></span>
        </div>
        <div class="nav">

                <?php wp_nav_menu('menu_class=menu&theme_location=top'); ?>
<!--            <svg>-->
<!--                <use xlink:href="--><?//= get_template_directory_uri(); ?><!--/img/sprite-inline.svg#dropdown"></use>-->
<!--            </svg>-->


            <div class="search">
                <div class="search-toggle">
                    <svg>
                        <use xlink:href="<?= get_template_directory_uri(); ?>/img/sprite-inline.svg#search"></use>
                    </svg>
                </div>
                <form class="search-box" action="<?php echo home_url('/') ?>" role="search" method="get">
                    <input class="search-input" type="text" name="s" id="s" placeholder="Search..." value="<?php echo get_search_query() ?>">
                </form>
            </div>
        </div>
    </div>
</header>