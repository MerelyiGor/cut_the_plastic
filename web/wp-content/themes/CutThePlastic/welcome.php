<?php
/*
 * Template Name: welcome page
*/

get_header();
the_post(); ?>

    <div class="main">
        <div class="bg-video">
            <img src="<?= get_the_post_thumbnail_url(get_the_ID(), 'full') ?>" alt="<?= the_title() ?>">
        </div>
        <div class="lg-container">
            <h1 class="welcome-title"><?= get_field('header_text') ?></h1>
        </div>
    </div>
    <section class="information">
        <div class="container">
            <div class="wrap">
                <?php
                if (get_field('add_text_block')) {
                    foreach (get_field('add_text_block') as $value) {
                        echo ($value['what_type_of_header'] == 'h1') ? '<h1>' . $value['heading'] . '</h1>' . $value['text'] : '';
                        echo ($value['what_type_of_header'] == 'h2') ? '<h2>' . $value['heading'] . '</h2>' . $value['text'] : '';
                        echo ($value['what_type_of_header'] == 'h3') ? '<h3>' . $value['heading'] . '</h3>' . $value['text'] : '';
                        echo ($value['what_type_of_header'] == 'h4') ? '<h4>' . $value['heading'] . '</h4>' . $value['text'] : '';
                        echo ($value['what_type_of_header'] == 'h5') ? '<h5>' . $value['heading'] . '</h5>' . $value['text'] : '';
                        echo ($value['what_type_of_header'] == 'h6') ? '<h6>' . $value['heading'] . '</h6>' . $value['text'] : '';
                    }
                } ?>

            </div>
        </div>
    </section>

<?php
get_footer();