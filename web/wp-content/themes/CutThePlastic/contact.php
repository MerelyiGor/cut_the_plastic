<?php
/*
 * Template Name: contact page
*/

get_header(); the_post();?>

    <div class="main">
        <div class="bg-video">
            <img src="<?= get_the_post_thumbnail_url(get_the_ID(),'full') ?>" alt="<?= the_title() ?>">
        </div>
        <div class="lg-container">
            <h1 class="welcome-title"><?= the_title() ?></h1>
        </div>
    </div>
    <section class="contact">
        <div class="container">
            <div class="wrap">
                <div class="row-flex">
                    <div class="col-md-7 col-xs-12">
                        <div class="contact-form">
                            <?= the_content(); ?>
                            <form action="">
                                <input type="text" name="name" placeholder="Name">
                                <input type="text" name="mail" placeholder="Email">
                                <input type="text" name="subject" placeholder="Subject">
                                <textarea name="textarea" cols="30" rows="10" placeholder="Subject"></textarea>
                                <button class="btn-send submit" type="submit">send</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-5 col-xs-12">
                        <div class="contact-image"><img src="<?= get_field('image_to_right') ?>" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php
get_footer();