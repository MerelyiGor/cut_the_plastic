<?php
/*
 * Template Name: The template for displaying search results pages.
*/

get_header();
the_post(); ?>
<div class="main">
    <div class="bg-video"><img src="<?= get_the_post_thumbnail_url(127, 'full') ?>" alt="<?= the_title() ?>"></div>
    <div class="lg-container">
        <h1><?= get_field('main_title', 127) ?></h1>
        <p><?= get_field('sub_title', 127) ?></p>
    </div>
</div>
<section class="searching">
    <div class="container">
        <div class="wrap">
            <div class="row-flex">
                <div class="col-md-7">
<!--                    --><?//= var_dump(implement_ajax()) ?>
                    <div class="searching-title">
                        <h2><?= get_field('title_to_search_input', 127) ?></h2>

                        <form class="searching-action" action="<?php echo home_url('/') ?>" role="search" method="get">
                            <input type="text" placeholder="SEARCH" name="s" id="s" value="<?php echo get_search_query() ?>">
                            <button type="submit">
                                <div class="search-toggle">
                                    <svg>
                                        <use xlink:href="<?= get_template_directory_uri(); ?>/img/sprite-inline.svg#search"></use>
                                    </svg>
                                </div>
                            </button>
                        </form>

                    </div>
                    <div class="searching-result"><? //= var_dump($wp_query->posts) ?><?= $wp_query->found_posts; ?> result found: «<?php the_search_query() ?>»</div>
                    <div class="searching-list">


                        <?php
//                        $args = array(
//                            'posts_per_page' => 5,
//                            'order' 	 => 'DESC',
//                            'publish' => true,
//                        );
//
//                        $my_query = new WP_Query( $args );

//                        if ( $my_query->have_posts() ) {
//                            while ( $my_query->have_posts() ) {
//
//                                $my_query->the_post();
//
//                                ?>
<!--                                <div class="searching-list--item">-->
<!--                                    <strong>-->
<!--                                        <a href="--><?php //the_permalink(); ?><!--">--><?php //the_title(); ?><!--</a>-->
<!--                                    </strong>-->
<!--                                    <a href="--><?php //the_permalink(); ?><!--">--><?php //the_permalink(); ?><!--</a>-->
<!--                                    <p>--><?//= mb_strcut(strip_tags(get_the_content()), 0, 250); ?><!--</p>-->
<!--                                </div>-->
<!--                                --><?php
//
//                            };
//                        };

                        //$GLOBALS['wp_query']->max_num_pages = $my_query->max_num_pages;


//                        wp_reset_query();

                        $num_post_show = 5;
                        implement_ajax($num_post_show); ?>


                    </div>
                </div>
                <div class="col-md-5"><img src="<?= get_field('image_in_the_right', 127) ?>" alt="<?= the_title() ?>"></div>
                <div class="col-md-12">
                    <div class="pagination">
                        <!--                        <div class="pagination-controls"><a class="prev" href="#"></a><a class="page active" href="#">1</a><a class="next" href="#"></a></div>-->
                        <button data-offset="<?= $num_post_show ?>" class="btnShowAll" onclick="btnShowAll(event)">Show All</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
