
$(document).ready(() => {
    $('.form-success-popup').fancybox({});
});

$('.contact-form form').submit(function(event){ //отслеживается первая форма которая вложена в элемент с классом .class
    event.preventDefault(); // также по мимо класса можно писать id ('#id form')

    var $this = $(this);

    $.post( myajax.url, { //обращение на передачу данных в myajax.url
        action: 'send_form', //название action который зарегистрирован для обработки данных в php
        name: $this.find('input[name="name"]').val(), //берутся значения по input и атрибуту name
        mail: $this.find('input[name="mail"]').val(),
        subject: $this.find('input[name="subject"]').val(),
        textarea: $this.find('textarea[name="textarea"]').val(),

    }, function(response) {
        let thanks = document.querySelector('.form-success-popup');
        $.fancybox.open(thanks);
        console.log('поехали бля')
        $this.get(0).reset(); // сбрасываю поля формы после отправки данных
        setTimeout(function () {
            $.fancybox.close(thanks);
        }, 5000); // закрываю pop-up2 если форма находится в pop-up
    });
});
$( ".success-ok" ).on( "click", function() {
    let thanks_ok = document.querySelector('.form-success-popup');
    $.fancybox.close(thanks_ok);
});










function loadProduct(event) {
    $(event.target).text('Loading...');
    console.log(this);
    var length__videos = $('.thumb-goods').length;

    var wp_data = {
        action: 'load_product',
        last: length__videos
    };
    $.ajax({
        url: myajax.url,
        data: wp_data,
        method: 'POST',
        success: function(response){
            response.data.products.map(renderProduct);
            wp_data.last = response.data.last;
            $(event.target).text('see all products');
        }
    })
}


function renderProduct(product){
    var html = "<div class=\"col-md-4 col-sm-6 col-xs-12\">\n" +
        "                    <div class=\"thumb-goods\">\n" +
        "                        <div class=\"thumb-goods__image\">\n" +
        "                            <img src=\""+product.img+"\" alt=\""+product.title+"\">\n" +
        "                        </div>\n" +
        "                        <div class=\"thumb-goods__description\">\n" +
        "                            <div class=\"thumb-goods__description-title\">"+ product.title+"</div>\n" +
        "                            <p>"+product.txt+"</p>\n" +
        "                            <div class=\"result\">\n" +
        "                                <span>$"+product.price+"</span>\n" +
        "                                <a class=\"btn btn-buy\" href=\""+product.amazon_url+"\">\n" +
        "                                    <img src=\"<?= get_template_directory_uri(); ?>/img/pages/elems/amazon_button.png\" alt=\"amazon\">\n" +
        "                                </a>\n" +
        "                            </div>\n" +
        "                        </div>\n" +
        "                    </div>\n" +
        "                </div>";
    $('.goods .lg-container .row-flex').append(html);
}

function loadVideo(event) {
    $(event.target).text('Loading...');
    var length__videos = $('.thumb-video').length;
    var wp_data = {
        action: 'load_video',
        last: length__videos
    };
    $.ajax({
        url: myajax.url,
        data: wp_data,
        method: 'POST',
        success: function(response){
            // console.log(response);
            response.data.videos.map(renderVideo);
            wp_data.last = response.data.last;
            $(event.target).text('see all videos');

        }
    })
}

function renderVideo(video){
    var html = "<div class=\"col-md-4 col-sm-6 col-xs-12\">\n" +
        "                    <div class=\"thumb-video\">\n" +
        "                        <div class=\"thumb-video__content\">\n" +
        "                            <a href=\"https://www.youtube.com/embed/"+video.code.id_to_video_on_youtube+"\">\n" +
        "                                <div class=\"thumb-video__content-time\"><span>"+video.content.time+"</span></div>\n" +
        "                                <div class=\"thumb-video__content-image\"><img src=\""+video.content.img+"\" alt=\"\"></div>\n" +
        "                                <div class=\"thumb-video__content-mouse\"><img src=\""+video.code.gif_animation_preview+"\" alt=\"\"></div>\n" +
        "                            </a>\n" +
        "                        </div>\n" +
        "                        <div class=\"thumb-video__description\">\n" +
        "                            <div class=\"thumb-video__description-title\">"+video.content.video_title+"</div>\n" +
        "                            <p>"+video.content.channel+"</p><span>"+video.content.views+" views</span><span>"+video.content.date+"</span>\n" +
        "                        </div>\n" +
        "                    </div>\n" +
        "                </div>";
    $('.videos .lg-container .row-flex').append(html);
}
















function btnShowAll(event) {
    $(event.target).text('Loading...');
    var length__videos = $('.thumb-goods').length;

    var wp_data = {
        action: 'load_post_search',
        offset: $('.btnShowAll').attr('data-offset')
    };

    $.ajax({
        url: myajax.url,
        data: wp_data,
        method: 'POST',
        success: function(response){
            $('.searching-list').append(response);
            $(event.target).text('Load more');
            $('.btnShowAll').attr('data-offset', $('.btnShowAll').attr('data-offset') + 5);
        }
    })
}












