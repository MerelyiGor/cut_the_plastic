<?php

/**
 * Функции темы
 * ---------------------------------------------------------------------------------------------------------------------
 */

/************** ------- Подключение файлов function ------- **************/
include_once 'inc/add-theme-support.php'; // Глобальные настройки темы и регистрация настроек
include_once 'inc/body-class.php'; // Присвоение кастомных класов для тега <body>
include_once 'inc/wp_json-oembed-fix_off.php'; // фикс wp_json и oembed из индексации - выключение REST API
include_once 'inc/get-post-settings.php'; // функции которые относятся к выводу постов на главной и странице рубрик
include_once 'inc/register-nav-menu.php'; // Регистрация меню + убираем контейнер + убираю классы и id элементов меню
include_once 'inc/rus-translit.php'; // Перевод урлов в транслит с русс названий
include_once 'inc/style-theme.php'; // Подключение стилей
include_once 'inc/function_ajax_load.php';
include_once 'inc/youtube_api_load.php';
include_once 'inc/ajax-form-message.php'; // Отправка данных AJAX из формы отправки данных
include_once 'inc/validator.php'; // Валидатор для проверки форм на email, phone, file --- использует отправку AJAX форм
//include_once 'inc/contact-form-7.php'; // Надстройки для плагина CF7
//include_once 'inc/custom-post-type.php'; // Кастомные посты
//include_once 'inc/acf-advanced-custom-fields.php'; // Настройки к плагину ACF кастомные поля
//include_once 'inc/avatar-wp-admin.php'; // Функция добавления своего аватара в админку для дефолтного отображения
//include_once 'inc/breadcrumb.php'; // Хлебные крошки
//include_once 'inc/get-comments.php'; // Получение коментариев
//include_once 'inc/json-parser-array.php'; // Работа с JSON масивами данных для вывода в верстку
//include_once 'inc/custom-paginations.php'; // Кастомная пагинация
//include_once 'inc/password-post.php'; // Запароленные записи
//include_once 'inc/post-hide.php'; // Скрытие постов учасников от других учасников которые не являются их авторами
//include_once 'inc/search-results-exclude.php'; // Поиск - исключение страниц и настройка
//include_once 'inc/sidebar-wiget.php'; // Сайт бар темы - для виджетов
//include_once 'inc/wp-admin-ccs.php'; // Добавление собственных стилей css для стр. регистрации, входа или админ панели
//include_once 'inc/wp-navigations-inc.php'; // Настройка пагинации 1 2 3 4 ...
//include_once 'inc/wpml-plugin-custm.php'; // WPML настройки кнопок переключения языков
//include_once 'inc/custom-registration-form.php'; // Кастомная форма регистрации
//include_once 'inc/taxonomy_custom_field.php'; // Кастомная форма регистрации


/************** ------- Ограничение на загрузку файлов - установка размера ------- **************/
add_filter('upload_size_limit', 'PBP_increase_upload');
function PBP_increase_upload($bytes)
{
    return 90048576; // 1 megabyte
}

/**
 * Отправка данных AJAX из формы отправки данных
 * ---------------------------------------------------------------------------------------------------------------------
 */

add_action('wp_enqueue_scripts', 'myajax_data', 99); //подключаю скрипты myajax к теме через свои скрипты - скрипты подключать только через  wp_enqueue_scripts
function myajax_data()
{


    wp_localize_script('vendor', 'myajax',  // подключаю myajax с скрипту с хендллером main.js
        array(
            'url' => admin_url('admin-ajax.php') //забираю данные js которые в js файле передаются в wp http:\/\/blufixx\/wp-admin\/admin-ajax.php
        )
    );

}


function custom_shortcode_img($atts)
{

    $count = '';
    extract(shortcode_atts(array(
        'count' => ''
    ), $atts));

    $count -= 1;

    $img = wp_get_attachment_image(get_field('add_image', get_queried_object()->ID)[$count]['img'], 'full', false, 'style=object-fit: contain;');

    return $img;
}

add_shortcode('img', 'custom_shortcode_img');


add_filter('pre_get_posts', function ($query) {

    if ($query->is_search) {
        $query->set('post_type', array('post', 'page'));
    }

    return $query;

});





function cf_search_join( $join ) {
    global $wpdb;

    if ( is_search() ) {
        $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
    }

    return $join;
}
add_filter('posts_join', 'cf_search_join' );

/**
 * Modify the search query with posts_where
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
 */
function cf_search_where( $where ) {
    global $pagenow, $wpdb;

    if ( is_search() ) {
        $where = preg_replace(
            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
    }

    return $where;
}
add_filter( 'posts_where', 'cf_search_where' );

/**
 * Prevent duplicates
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
 */
function cf_search_distinct( $where ) {
    global $wpdb;

    if ( is_search() ) {
        return "DISTINCT";
    }

    return $where;
}
add_filter( 'posts_distinct', 'cf_search_distinct' );