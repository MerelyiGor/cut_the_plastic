<?php
/**
 * The main template file
 */

get_header(); ?>

<?php get_template_part( 'partials/header-video' ); ?>

<?php get_template_part( 'partials/must-see-videos' ); ?>

<?php get_template_part( 'partials/products-plastic' ); ?>

<?php get_footer(); ?>
