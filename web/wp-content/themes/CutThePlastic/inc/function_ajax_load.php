<?php
function load_video_callback()
{
    $last = $_REQUEST['last'];
    $videos = get_field('add_video_block', 2);
    $api_key = get_field('api_key', 2);

    $result = [];
    for ($WP_Query = $last; $last < $WP_Query + 9; $last++) {
        if (!isset($videos[$last])) {
            $last--;
            break;
        }
        $result[] = ['code' => $videos[$last], 'content' => load_api_youtube($videos[$last]['id_to_video_on_youtube'], $api_key)];
    }

    return wp_send_json_success(['videos' => $result, 'last' => $last]);

}

add_action('wp_ajax_load_video', 'load_video_callback');
add_action('wp_ajax_nopriv_load_video', 'load_video_callback');


function load_product_callback()
{
    $last = $_REQUEST['last'];
    $products = get_field('add_product', 2);

    $result = [];
    for ($WP_Query = $last; $last < $WP_Query + 9; $last++) {
        if (!isset($products[$last])) {
            $last--;
            break;
        }
        $result[] = $products[$last];
    }
    return wp_send_json_success(['products' => $result, 'last' => $last]);

}

add_action('wp_ajax_load_product', 'load_product_callback');
add_action('wp_ajax_nopriv_load_product', 'load_product_callback');


function load_post_search_callback()
{

    implement_ajax(5,$_REQUEST['offset']);
    wp_die();
}

add_action('wp_ajax_load_post_search', 'load_post_search_callback');
add_action('wp_ajax_nopriv_load_post_search', 'load_post_search_callback');


function implement_ajax($num_show_post,$offset = 0)
{
    $paged = 1;
    if (isset($_GET['paged']))
        $paged = $_GET['paged'];

    $WP_Query = new WP_Query(array(
            'post_type' => 'any',
            's' => $_GET['s'],
            'posts_per_page' => $num_show_post,
            'offset' => $offset
    ));


    if ($WP_Query->have_posts()) {
         while ($WP_Query->have_posts()) {
            $WP_Query->the_post();
            ?>
             <?php
//global $wpdb;
//             $result = $wpdb->get_results( "SELECT * FROM wp_posts WHERE post_content LIKE '%phrase%'" );
//             echo '<pre>';
//             var_dump($result);
//             echo '<pre>';
//             if ($result){
//                 foreach($result as $pageThing){
//                     echo $pageThing->post_content;
//                 }
//             }
             ?>

            <div class="searching-list--item">
                <strong>
                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                </strong>
                <a href="<?php the_permalink(); ?>"><?php the_permalink(); ?></a>
                <p><?= mb_strcut(strip_tags(get_the_content()), 0, 250); ?></p>
            </div>

        <?php }
    }
}

add_action('wp_ajax_nopriv_implement_ajax', 'implement_ajax');
add_action('wp_ajax_implement_ajax', 'implement_ajax');