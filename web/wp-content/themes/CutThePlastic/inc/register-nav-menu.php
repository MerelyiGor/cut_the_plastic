<?php
/**
 * Регистрация меню + убираем контейнер + убираю классы и id элементов меню
 * в данном примере регистрирует три меню - Главное меню - Верхнее меню - Нижнее меню
 * ---------------------------------------------------------------------------------------------------------------------
 */


add_action('after_setup_theme', function(){
    register_nav_menus(array(
        'top'    => 'top menu location',    //Название месторасположения меню в шаблоне - theme_location
        'bottom' => 'bottom menu location'      //Название другого месторасположения меню в шаблоне - theme_location
    ) );
});



add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args');
function my_wp_nav_menu_args($args = '')
{
	$args['container'] = false; //убираем контейнер
	return $args;
}
