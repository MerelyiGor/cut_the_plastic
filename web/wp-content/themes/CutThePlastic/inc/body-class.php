<?php

function custom_body_class($wp_classes)
{
    if (is_front_page()) {
        $wp_classes[] = 'page-index';
    }

    if (is_page(3)) {
        $wp_classes[] = 'page-welcome';
    }

    if (is_search()) {
        $wp_classes[] = 'page-search';
    }


    foreach($wp_classes as &$str){
        if(strpos($str, "search") > -1){
            $str = "";
        }
    }

    return $wp_classes;
}

add_filter('body_class', 'custom_body_class', 10, 2);
