<?php

function NormalizeDuration($duration){
    $start = new \DateTime('@0');
    $start->add(new \DateInterval($duration));

    return $start->format('H:i:s'); //format
}

function load_api_youtube($videoID, $api_key)
{

    //$videoID = 'FSt5IMOM2x0';
    //$api_key = 'AIzaSyBahdejj6gauMq0yTyJ0_MIQid_63MMWCA';
    $json = file_get_contents("https://www.googleapis.com/youtube/v3/videos?part=statistics,contentDetails,snippet&id=" . $videoID . "&key=" . $api_key);
    $jsonData = json_decode($json);
    $views = number_format($jsonData->items[0]->statistics->viewCount);
    $date = $jsonData->items[0]->snippet->publishedAt;
    $thumbnails = $jsonData->items[0]->snippet->thumbnails->medium->url;
    $time = $jsonData->items[0]->contentDetails->duration;
    $channel = $jsonData->items[0]->snippet->channelTitle;
    $video_title = $jsonData->items[0]->snippet->title;

    $somedate = sprintf('%s ago', human_time_diff(strtotime($date)));

    $times = NormalizeDuration($time);

    $array = [
        'views' => $views,
        'date' => $somedate,
        'channel' => $channel,
        'video_title' => $video_title,
        'img' => $thumbnails,
        'time' => $times
    ];

    return $array;
}