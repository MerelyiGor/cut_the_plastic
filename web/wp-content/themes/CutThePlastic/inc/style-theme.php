<?php
/**
 * функция подключение (стили / скрипты) на все страницы
 * ---------------------------------------------------------------------------------------------------------------------
 */

function CutThePlastic()
{

    wp_enqueue_style('style', get_template_directory_uri() . '/style.css', array(), time());
    wp_enqueue_style('vendor', get_template_directory_uri() . '/css/vendor.css', array(), time());
    wp_enqueue_style('main', get_template_directory_uri() . '/css/main.css', array(), time());

    wp_enqueue_script('vendor', get_template_directory_uri() . '/js/vendor.js', array(), time(), true);
//    wp_enqueue_script('youtube', '//www.youtube.com/iframe_api', array(), time(), true);
//    wp_enqueue_script('youtube', '//s.ytimg.com/yts/jsbin/www-widgetapi-vflvi9no-/www-widgetapi.js', array(), time(), true);
    wp_enqueue_script('main', get_template_directory_uri() . '/js/main.js', array(), time(), true);
    wp_enqueue_script('custom', get_template_directory_uri() . '/js/custom.js', array(), time(), true);

}
add_action('wp_enqueue_scripts', 'CutThePlastic');