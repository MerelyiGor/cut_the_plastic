<?php
/**
 * Отправка данных AJAX из формы отправки данных
 * ---------------------------------------------------------------------------------------------------------------------
 */


add_action('wp_ajax_send_form', 'send_form_callback');
add_action('wp_ajax_nopriv_send_form', 'send_form_callback');

function send_form_callback() {
    $name = $_POST['name'];
    $mail = $_POST['mail'];
    $subject = $_POST['subject'];
    $textarea = $_POST['textarea'];
    $email = get_field('mail_to', 90); // 'name@mail.ru'
    $response = [];

    $fields = array( //загоняю значения $name и $textarea в $fields
        'required' => array(
            'name' => $name,
            'mail' => $mail,
        )
    );


    $response['errors'] = Validator::validate_fields($fields);

    if (count($response['errors'])) {
        wp_send_json_error($response);
        return;
    }

    $message = "Name: $name\n" . "Email: $mail\n" . "Subject: $subject\n" . "Message: $textarea\n";


    $send_success = wp_mail($email, 'Email sent from CutThePlastic website', $message);

    if (!$send_success) {
        wp_send_json_error("WP_mail not send");
    }

    wp_send_json_success($response);
    die();
}
