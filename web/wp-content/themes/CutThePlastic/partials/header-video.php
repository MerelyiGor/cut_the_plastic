<div class="main">
    <div class="bg-video">
        <video preload id="video-title">
            <source src="<?= get_field('video_to_header', 2); ?>" type="video/mp4">
            <source src="<?= get_field('video_to_header', 2); ?>" type="video/ogg">
        </video>
    </div>
    <div class="lg-container">
        <h1><?= get_field('header_settings')['setting_title_in_the_header']; ?></h1>
        <p><?= get_field('header_settings')['sub_tittle_header_video']; ?></p>
    </div>
</div>