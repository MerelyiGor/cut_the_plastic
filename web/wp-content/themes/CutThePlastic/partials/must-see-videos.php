<section class="videos">
    <div class="lg-container">
        <h2>Must-See Videos</h2>
        <div class="row-flex">
            <?php $api_key = get_field('api_key', 2); ?>

            <?php $i = 0;
            foreach (get_field('add_video_block', 2) as $key => $value): $i++;
                if ($i > 9) continue;

                $args_video = load_api_youtube($value['id_to_video_on_youtube'], $api_key)
                ?>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="thumb-video">
                        <div class="thumb-video__content">
                            <a href="https://www.youtube.com/embed/<?= $value['id_to_video_on_youtube'] ?>">
                                <div class="thumb-video__content-time"><span><?= $args_video['time'] ?></span></div>
                                <div class="thumb-video__content-image"><img src="<?= $args_video['img'] ?>" alt=""></div>
                                <div class="thumb-video__content-mouse"><img src="<?= $value['gif_animation_preview'] ?>" alt=""></div>
                            </a>
                        </div>
                        <div class="thumb-video__description">
                            <div class="thumb-video__description-title"><?= $args_video['video_title'] ?></div>
                            <p><?= $args_video['channel'] ?></p><span><?= $args_video['views'] ?> views</span><span><?= $args_video['date'] ?></span>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>
        <button class="btn btn-more" onclick="loadVideo(event)">see all videos</button>
    </div>
</section>