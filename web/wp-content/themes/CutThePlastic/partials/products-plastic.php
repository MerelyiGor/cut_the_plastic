
<section class="goods">
    <div class="lg-container">
        <h2>Products That Reduce Your Plastic Waste</h2>
        <div class="row-flex">

            <?php $i = 0;
            foreach (get_field('add_product',2) as $key => $value): $i++;
            if ($i > 9) continue; ?>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="thumb-goods">
                        <div class="thumb-goods__image">
                            <img src="<?= $value['img'] ?>" alt="<?= $value['title'] ?>">
                        </div>
                        <div class="thumb-goods__description">
                            <div class="thumb-goods__description-title"><?= $value['title'] ?></div>
                            <p><?= $value['txt'] ?></p>
                            <div class="result">
                                <span>$<?= $value['price'] ?></span>
                                <a class="btn btn-buy" href="<?= $value['amazon_url'] ?>">
                                    <img src="<?= get_template_directory_uri(); ?>/img/pages/elems/amazon_button.png" alt="amazon">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
            
        </div>
        <button class="btn btn-more" onclick="loadProduct(event)">see all products</button>
    </div>
</section>