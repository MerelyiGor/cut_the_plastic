<?php
/**
 * The template for displaying the footer
 */
?>

<footer class="footer">
    <div class="container">
        <div class="footer-navigation">
            <div class="row-flex">
                <div class="col-md-4 footer-about">
                    <div class="footer-about__text">
                        <?= get_field('footer', 2)['text_on_the_left']; ?>
                    </div>
                </div>
                <div class="col-md-4 footer-content">
                    <div class="footer-content__text">
                        <h6><?= get_field('footer', 2)['title_menu_bottom']; ?></h6>
                        <?php wp_nav_menu('theme_location=bottom'); ?>
                    </div>
                </div>
                <div class="col-md-4 footer-social">
                    <div class="footer-social__text">
                        <h6><?= get_field('footer', 2)['title_menu_bottom_social']; ?></h6>
                        <ul>
                            <li>
                                <a href="<?= get_field('footer', 2)['link_to_facebook']; ?>">
                                    <i class="fa fa-facebook">
                                        <svg>
                                            <use xlink:href="<?= get_template_directory_uri(); ?>/img/sprite-inline.svg#facebook-f"></use>
                                        </svg>
                                    </i>
                                </a>
                            </li>
                            <li>
                                <a href="<?= get_field('footer', 2)['link_to_twitter']; ?>">
                                    <i class="fa fa-twitter">
                                        <svg>
                                            <use xlink:href="<?= get_template_directory_uri(); ?>/img/sprite-inline.svg#twitter"></use>
                                        </svg>
                                    </i>
                                </a>
                            </li>
<!--                            <li>-->
<!--                                <a href="--><?//= get_field('footer', 2)['link_to_google_plus']; ?><!--">-->
<!--                                    <i class="fa fa-google-plus">-->
<!--                                        <svg>-->
<!--                                            <use xlink:href="--><?//= get_template_directory_uri(); ?><!--/img/sprite-inline.svg#iconmonstr-google-plus-1"></use>-->
<!--                                        </svg>-->
<!--                                    </i>-->
<!--                                </a>-->
<!--                            </li>-->
                            <li>
                                <a href="<?= get_field('footer', 2)['link_to_pinterest']; ?>">
                                    <i class="fa fa-pinterest">
                                        <svg>
                                            <use xlink:href="<?= get_template_directory_uri(); ?>/img/sprite-inline.svg#iconmonstr-pinterest-1"></use>
                                        </svg>
                                    </i>
                                </a>
                            </li>
                            <li>
                                <a href="<?= get_field('footer', 2)['link_to_instagram']; ?>">
                                    <i class="fa fa-instagram">
                                        <svg>
                                            <use xlink:href="<?= get_template_directory_uri(); ?>/img/sprite-inline.svg#iconmonstr-instagram-11"></use>
                                        </svg>
                                    </i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <p><?= get_field('footer', 2)['text_to_copyright']; ?></p>
            <?= get_field('footer', 2)['deep_bottom_text_to_footer']; ?>
        </div>
    </div>
</footer>
<div class="form-success-popup">
    <div class="form-success-popup-inner">
        <img class="right-icon" src="<?= get_template_directory_uri() ?>/img/right-icon.png" alt="" />
        <h3><?= get_field('text_to_popup_window', 90)['text_1']; ?></h3>
        <p><?= get_field('text_to_popup_window', 90)['text_2']; ?></p>
        <a href="#" class="success-ok"><?= get_field('text_to_popup_window', 90)['text_3']; ?></a>
    </div>
</div>

<?php wp_footer(); ?>

</body>
</html>
